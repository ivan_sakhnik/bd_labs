#My Database
#lab1___variant-17
import function
import pickle

ch = '';
f = open ("database.pickle", "rb")
database = pickle.load(f)

while ch != '5':
    print ('1:. show database')
    print ('2:. Olympics where there are team sports')
    print ('3:. add Olympics to the database')
    print ('4:. remove Olympics from the database')
    print ('5:. Exit')
    
    ch = raw_input()
   
    if ch == '1':
        function.ShowDatabase(database)
    elif ch == '2':
        function.MyVariant(database)
    elif ch == '3':
        function.AddElem(database)
    elif ch == '4':
        function.DelElem(database)
    elif ch == '5':
        exit;
    else:
        print('Enter the correct value')
else:
    f = open ("database.pickle", "wb")
    pickle.dump(database,f)
    f.close()
    print('____Program completed____')

