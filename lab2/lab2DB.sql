-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 11 2016 г., 00:46
-- Версия сервера: 5.5.45
-- Версия PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `lab2DB`
--
CREATE DATABASE IF NOT EXISTS `lab2DB` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `lab2DB`;

-- --------------------------------------------------------

--
-- Структура таблицы `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `departments`
--

INSERT INTO `departments` (`id`, `name`) VALUES
(1, 'department1'),
(2, 'department2'),
(3, 'department3');

--
-- Триггеры `departments`
--
DROP TRIGGER IF EXISTS `delProductsByDepartment`;
DELIMITER //
CREATE TRIGGER `delProductsByDepartment` BEFORE DELETE ON `departments`
 FOR EACH ROW delete from `products` where `products`.`department` = old.`id`
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `orderers`
--

CREATE TABLE IF NOT EXISTS `orderers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `surname` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`surname`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `orderers`
--

INSERT INTO `orderers` (`id`, `name`, `surname`) VALUES
(1, 'Name1', 'Surname1'),
(2, 'Name2', 'Surname2'),
(3, 'Name3', 'Surname3');

--
-- Триггеры `orderers`
--
DROP TRIGGER IF EXISTS `delOrdersByOrderer`;
DELIMITER //
CREATE TRIGGER `delOrdersByOrderer` BEFORE DELETE ON `orderers`
 FOR EACH ROW delete from `orders` where `orders`.`orderer` = old.`id`
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product` int(10) NOT NULL,
  `count` int(10) NOT NULL,
  `orderer` int(10) NOT NULL,
  `terms` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product` (`product`),
  KEY `orderer` (`orderer`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `product`, `count`, `orderer`, `terms`) VALUES
(2, 2, 1, 3, '11.10.2016'),
(3, 3, 10, 2, '10.09.2016'),
(4, 4, 3, 1, '25.11.2016');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product` varchar(30) NOT NULL,
  `cost` int(10) NOT NULL,
  `department` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product` (`product`),
  UNIQUE KEY `product_2` (`product`),
  KEY `department` (`department`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `product`, `cost`, `department`) VALUES
(1, 'product1', 1000, 1),
(2, 'product2', 2500, 2),
(3, 'product3', 1500, 1),
(4, 'product4', 5000, 3);

--
-- Триггеры `products`
--
DROP TRIGGER IF EXISTS `delOrdersByProduct`;
DELIMITER //
CREATE TRIGGER `delOrdersByProduct` BEFORE DELETE ON `products`
 FOR EACH ROW delete from `orders` where `product` = old.`id`
//
DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
